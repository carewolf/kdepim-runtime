# Irish translation of akonadi_google_resource
# Copyright (C) 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the akonadi_google_resource package.
# Kevin Scannell <kscanne@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: akonadi_google_resource\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-30 01:38+0000\n"
"PO-Revision-Date: 2012-05-14 16:25-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#: calendarhandler.cpp:90
#, kde-format
msgctxt "@info:status"
msgid "Retrieving calendars"
msgstr ""

#: calendarhandler.cpp:141
#, kde-format
msgctxt "@info:status"
msgid "Retrieving events for calendar '%1'"
msgstr ""

#: calendarhandler.cpp:197 taskhandler.cpp:170
#, kde-format
msgctxt "@info:status"
msgid "Adding event to calendar '%1'"
msgstr ""

#: calendarhandler.cpp:221
#, kde-format
msgctxt "@info:status"
msgid "Changing event in calendar '%1'"
msgstr ""

#: calendarhandler.cpp:232
#, kde-format
msgctxt "@info:status"
msgid "Removing %1 event"
msgid_plural "Removing %1 events"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: calendarhandler.cpp:248
#, kde-format
msgctxt "@info:status"
msgid "Moving %1 event from calendar '%2' to calendar '%3'"
msgid_plural "Moving %1 events from calendar '%2' to calendar '%3'"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: calendarhandler.cpp:266
#, kde-format
msgctxt "@info:status"
msgid "Creating calendar '%1'"
msgstr ""

#: calendarhandler.cpp:294
#, kde-format
msgctxt "@info:status"
msgid "Changing calendar '%1'"
msgstr ""

#: calendarhandler.cpp:307
#, kde-format
msgctxt "@info:status"
msgid "Removing calendar '%1'"
msgstr ""

#: generichandler.cpp:24
#, kde-format
msgid "Cannot handle item linking"
msgstr ""

#: generichandler.cpp:29
#, kde-format
msgid "Cannot handle item unlinking"
msgstr ""

#: generichandler.cpp:56
#, kde-format
msgctxt "@status"
msgid "Ready"
msgstr ""

#: generichandler.h:65 generichandler.h:77
#, kde-format
msgid "Invalid item."
msgstr ""

#: googleresource.cpp:82
#, kde-format
msgid "Fetching password..."
msgstr ""

#: googleresource.cpp:89
#, kde-format
msgid "Can't fetch password"
msgstr ""

#: googleresource.cpp:130
#, kde-format
msgctxt "@info:status"
msgid "Ready"
msgstr ""

#: googleresource.cpp:142
#, kde-format
msgid "Not configured"
msgstr ""

#: googleresource.cpp:142
#, kde-format
msgctxt "%1 is account name (user@gmail.com)"
msgid "Google Groupware (%1)"
msgstr ""

#: googleresource.cpp:193
#, kde-format
msgid "Account has been logged out."
msgstr ""

#: googleresource.cpp:196
#, kde-format
msgid ""
"Google Groupware has been logged out from your account. Please log in to "
"enable Google Contacts and Calendar sync again."
msgstr ""

#: googleresource.cpp:197
#, kde-format
msgid ""
"Google Groupware has been logged out from account %1. Please log in to "
"enable Google Contacts and Calendar sync again."
msgstr ""

#: googleresource.cpp:201
#, kde-format
msgctxt "@title"
msgid "%1 needs your attention."
msgstr ""

#: googleresource.cpp:206
#, kde-format
msgctxt "@action"
msgid "Log in"
msgstr ""

#: googleresource.cpp:218 googleresource.cpp:219
#, kde-format
msgctxt "@info:status"
msgid "Resource is not configured"
msgstr ""

#: googleresource.cpp:234
#, kde-format
msgid "Failed to refresh tokens"
msgstr ""

#: googleresource.cpp:423 googleresource.cpp:437 googleresource.cpp:451
#: googleresource.cpp:466 googleresource.cpp:480 googleresource.cpp:494
#, kde-format
msgid "Invalid payload type"
msgstr ""

#: googleresource.cpp:508 googleresource.cpp:522 googleresource.cpp:536
#, kde-format
msgid "Unknown collection mimetype"
msgstr ""

#: googlesettingswidget.cpp:40
#, kde-format
msgid " minute"
msgid_plural " minutes"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#. i18n: ectx: property (text), widget (QLabel, accountLabel)
#: googlesettingswidget.cpp:102 googlesettingswidget.ui:30
#, kde-format
msgid "<b>Not configured</b>"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, accountTextLabel)
#: googlesettingswidget.ui:23
#, fuzzy, kde-format
#| msgid "Accounts"
msgid "Account:"
msgstr "Cuntais"

#. i18n: ectx: property (text), widget (QPushButton, configureBtn)
#: googlesettingswidget.ui:42
#, kde-format
msgid "Configure..."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, enableRefresh)
#: googlesettingswidget.ui:49
#, kde-format
msgid "Enable interval refresh"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, refreshLabel)
#: googlesettingswidget.ui:56
#, kde-format
msgid "Refresh interval:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, eventsLimitLabel)
#: googlesettingswidget.ui:79
#, kde-format
msgid "Fetch only events since:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, calendarsLabel)
#: googlesettingswidget.ui:92
#, fuzzy, kde-format
#| msgid "Calendars"
msgid "Enabled Calendars:"
msgstr "Féilirí"

#. i18n: ectx: property (text), widget (QPushButton, reloadCalendarsBtn)
#. i18n: ectx: property (text), widget (QPushButton, reloadTaskListsBtn)
#: googlesettingswidget.ui:123 googlesettingswidget.ui:166
#, kde-format
msgid "Reload"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, taskListsLabel)
#: googlesettingswidget.ui:135
#, kde-format
msgid "Enabled Tasklists:"
msgstr ""

#: personhandler.cpp:86
#, kde-format
msgctxt "Name of a group of contacts"
msgid "Coworkers"
msgstr ""

#: personhandler.cpp:88
#, kde-format
msgctxt "Name of a group of contacts"
msgid "Friends"
msgstr ""

#: personhandler.cpp:90
#, kde-format
msgctxt "Name of a group of contacts"
msgid "Family"
msgstr ""

#: personhandler.cpp:92
#, kde-format
msgctxt "Name of a group of contacts"
msgid "My Contacts"
msgstr ""

#: personhandler.cpp:116
#, kde-format
msgctxt "@info:status"
msgid "Retrieving contacts groups"
msgstr ""

#: personhandler.cpp:123 personhandler.cpp:129
#, kde-format
msgid "Other Contacts"
msgstr ""

#: personhandler.cpp:170
#, kde-format
msgctxt "@info:status"
msgid "Retrieving contacts for group '%1'"
msgstr ""

#: personhandler.cpp:374
#, kde-format
msgctxt "@info:status"
msgid "Adding contact to group '%1'"
msgstr ""

#: personhandler.cpp:408
#, kde-format
msgctxt "@info:status"
msgid "Changing contact"
msgstr ""

#: personhandler.cpp:422
#, kde-format
msgctxt "@info:status"
msgid "Removing %1 contact"
msgid_plural "Removing %1 contacts"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: personhandler.cpp:442
#, fuzzy, kde-format
#| msgid "Invalid collection"
msgid "Invalid source or destination collection"
msgstr "Bailiúchán neamhbhailí"

#: personhandler.cpp:448
#, kde-format
msgctxt "@info:status"
msgid "Moving %1 contact from group '%2' to '%3'"
msgid_plural "Moving %1 contacts from group '%2' to '%3'"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: personhandler.cpp:465
#, kde-format
msgctxt "@info:status"
msgid "Linking %1 contact"
msgid_plural "Linking %1 contacts"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: personhandler.cpp:479
#, kde-format
msgctxt "@info:status"
msgid "Unlinking %1 contact"
msgid_plural "Unlinking %1 contacts"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: personhandler.cpp:512
#, kde-format
msgctxt "@info:status"
msgid "Creating new contact group '%1'"
msgstr ""

#: personhandler.cpp:533
#, kde-format
msgctxt "@info:status"
msgid "Changing contact group '%1'"
msgstr ""

#: personhandler.cpp:558
#, kde-format
msgctxt "@info:status"
msgid "Removing contact group '%1'"
msgstr ""

#. i18n: ectx: label, entry (Calendars), group (General)
#: settingsbase.kcfg:21
#, kde-format
msgid "IDs of calendars in collection"
msgstr ""

#. i18n: ectx: label, entry (TaskLists), group (General)
#: settingsbase.kcfg:25
#, kde-format
msgid "IDs of task lists in collection"
msgstr ""

#: taskhandler.cpp:70
#, kde-format
msgctxt "@info:status"
msgid "Retrieving task lists"
msgstr ""

#: taskhandler.cpp:103
#, kde-format
msgctxt "@info:status"
msgid "Retrieving tasks for list '%1'"
msgstr ""

#: taskhandler.cpp:195
#, kde-format
msgctxt "@info:status"
msgid "Changing task in list '%1'"
msgstr ""

#: taskhandler.cpp:215
#, kde-format
msgctxt "@info:status"
msgid "Removing %1 task"
msgid_plural "Removing %1 tasks"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: taskhandler.cpp:225
#, kde-format
msgid "Failed to delete task: %1"
msgstr ""

#: taskhandler.cpp:261
#, kde-format
msgid "Failed to reparent subtasks: %1"
msgstr ""

#: taskhandler.cpp:292
#, kde-format
msgid "Moving tasks between task lists is not supported"
msgstr ""

#: taskhandler.cpp:297
#, kde-format
msgctxt "@info:status"
msgid "Creating new task list '%1'"
msgstr ""

#: taskhandler.cpp:322
#, kde-format
msgctxt "@info:status"
msgid "Changing task list '%1'"
msgstr ""

#: taskhandler.cpp:335
#, kde-format
msgctxt "@info:status"
msgid "Removing task list '%1'"
msgstr ""

#~ msgid "Aborted"
#~ msgstr "Tobscortha"

#, fuzzy
#~| msgid "Remove"
#~ msgid "&Remove"
#~ msgstr "Bain"

#, fuzzy
#~| msgid "Account"
#~ msgid "Unknown Account"
#~ msgstr "Cuntas"

#~ msgid "Description:"
#~ msgstr "Cur Síos:"

#~ msgid "Location:"
#~ msgstr "Suíomh:"

#~ msgid "Timezone:"
#~ msgstr "Crios ama:"

#~ msgid "Add"
#~ msgstr "Cuir Leis"

#~ msgid "Edit"
#~ msgstr "Eagar"

#~ msgid "An error occurred"
#~ msgstr "Tharla earráid"
