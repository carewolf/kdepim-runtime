# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrian Chaves Fernandez <adriyetichaves@gmail.com>, 2013, 2015, 2016, 2017.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-25 00:47+0000\n"
"PO-Revision-Date: 2023-06-20 08:02+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: icaldirresource.cpp:121
#, kde-format
msgid "Incidence with uid '%1' not found."
msgstr "Non se atopou ningunha incidencia co identificador de usuario «%1»."

#: icaldirresource.cpp:135 icaldirresource.cpp:168 icaldirresource.cpp:200
#, kde-format
msgid "Trying to write to a read-only directory: '%1'"
msgstr "Intentando escribir nun directorio só de lectura: «%1»."

#: icaldirresource.cpp:250
#, kde-format
msgid "Calendar Folder"
msgstr "Cartafol do calendario"

#. i18n: ectx: label, entry (Path), group (General)
#: icaldirresource.kcfg:10
#, kde-format
msgid "Path to iCal directory"
msgstr "Ruta ao directorio iCal"

#. i18n: ectx: label, entry (AutosaveInterval), group (General)
#: icaldirresource.kcfg:14
#, kde-format
msgid "Autosave interval time (in minutes)."
msgstr "Intervalo de garda automática (en minutos)."

#. i18n: ectx: label, entry (ReadOnly), group (General)
#: icaldirresource.kcfg:18
#, kde-format
msgid "Do not change the actual backend data."
msgstr "Non cambiar os datos de verdade da infraestrutura."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: icaldirsagentsettingswidget.ui:33
#, kde-format
msgid "Directory"
msgstr "Directorio"

#. i18n: ectx: property (text), widget (QLabel, label)
#: icaldirsagentsettingswidget.ui:39
#, kde-format
msgid "Di&rectory:"
msgstr "Di&rectorio:"

#. i18n: ectx: property (text), widget (QLabel, pathLabel)
#: icaldirsagentsettingswidget.ui:58
#, kde-format
msgid ""
"Select the directory whose contents should be represented by this resource. "
"If the directory does not exist, it will be created."
msgstr ""
"Seleccione o directorio cuxo contido debería estar representado por este "
"recurso. Se o directorio non existe, crearase."

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ReadOnly)
#: icaldirsagentsettingswidget.ui:68
#, kde-format
msgid "Read only"
msgstr "Só lectura"

#. i18n: ectx: property (text), widget (QLabel, readOnlyLabel)
#: icaldirsagentsettingswidget.ui:84
#, kde-format
msgid ""
"When read-only mode is enabled, no changes will be written. Read-only mode "
"is automatically enabled when the selected file does not support write "
"access."
msgstr ""
"Cando o modo só de lectura está activado, non se escribirán cambios. O modo "
"só de lectura actívase automaticamente cando o ficheiro seleccionado non "
"permite acceso de escritura."

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: icaldirsagentsettingswidget.ui:108
#, kde-format
msgid "Tuning"
msgstr "Axustes"

#. i18n: ectx: property (text), widget (QLabel, runingLabel)
#: icaldirsagentsettingswidget.ui:114
#, kde-format
msgid ""
"The options on this page allow you to change parameters that balance data "
"safety and consistency against performance. In general you should be careful "
"with changing anything here, the defaults are good enough in most cases."
msgstr ""
"As opcións desta páxina permiten cambiar os parámetros que manteñen o "
"equilibrio entre seguridade dos datos e consistencia para o rendemento. En "
"xeral, debería ter moito coidado ao cambiar cousas de aquí, os valores "
"predeterminados son axeitados para case todas as circunstancias."

#. i18n: ectx: property (text), widget (QLabel, autosaveLabel)
#: icaldirsagentsettingswidget.ui:124
#, kde-format
msgid "Autosave delay:"
msgstr "Atraso de garda automática:"

#: icaldirsettingswidget.cpp:36
#, kde-format
msgid " minute"
msgid_plural " minutes"
msgstr[0] " minuto"
msgstr[1] " minutos"

#~ msgid "Directory Name"
#~ msgstr "Nome do directorio"

#~ msgid "Access Rights"
#~ msgstr "Dereitos de acceso"

#, fuzzy
#~| msgid "Calendar Folder"
#~ msgid "Calendar Name:"
#~ msgstr "Cartafol do calendario"
